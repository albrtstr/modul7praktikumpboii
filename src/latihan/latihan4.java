/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package latihan;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class latihan4 {
    
    public static void main(String[] args) {
        ArrayList<Integer> deret = new ArrayList<Integer>();
        
        for (int i = 0; i < 10; i++) {
            deret.add(i * (i * 2));
        }
//        deret.add(0, 18);
//        deret.add(5, 52);
//        deret.add(6, 128);
        System.out.println(deret.size());
        System.out.println(deret);
        
        System.out.println(deret.indexOf(18));
        System.out.println(deret.indexOf(52));
        System.out.println(deret.indexOf(128));
        
        System.out.println("\nMemeriksa keberadaan suatu nilai di dalam array list = ");
        System.out.println(deret.contains(18));
        System.out.println(deret.contains(52));
        System.out.println(deret.contains(128));
    }
}
