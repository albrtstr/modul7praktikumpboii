/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package latihan;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class latihan2 {

    public static void main(String[] args) {
        ArrayList<Integer> deret = new ArrayList<Integer>();
        System.out.println("Panjang deret awal : " + deret.size());

        for (int i = 0; i < 10; i++) {
            deret.add(i * (i + 10));
        }

        System.out.println("\nPanjang deret setelah ditambahkan elemen: " + deret.size());

    }
}
