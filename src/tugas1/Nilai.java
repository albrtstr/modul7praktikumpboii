/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas1;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class Nilai {

    ArrayList<Mahasiswa> tampung;
    Mahasiswa nilai;
    double Ratarata;

    public Nilai() {
        tampung = new ArrayList<Mahasiswa>();
    }

    public void isiData(String Nama, String NIM, double Nilai) {
        tampung.add(new Mahasiswa(Nama, NIM, Nilai));
    }

    public void showData() {
        for (Mahasiswa nilai : tampung) {
            System.out.println("Nama = " + nilai.getNama()
                    + ", NIM = " + nilai.getNIM()
                    + ", Nilai = " + nilai.getNilai());
        }
    }

    public double showRatarata() {
        int sum = 0;
        for (Mahasiswa nilai : tampung) {
            sum += nilai.getNilai();
        }
        return sum / tampung.size();
    }
}
