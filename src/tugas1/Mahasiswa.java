/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas1;

/**
 *
 * @author ASUS
 */
public class Mahasiswa {

    private String Nama;
    private String NIM;
    private double  Nilai;

    public Mahasiswa() {

    }

    public Mahasiswa(String Nama, String NIM, double Nilai) {
        this.Nama = Nama;
        this.NIM = NIM;
        this.Nilai = Nilai;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String Nama) {
        this.Nama = Nama;
    }

    public String getNIM() {
        return NIM;
    }

    public void setNIM(String NIM) {
        this.NIM = NIM;
    }

    public double getNilai() {
        return Nilai;
    }

    public void setNilai(double Nilai) {
        this.Nilai = Nilai;
    }

}
