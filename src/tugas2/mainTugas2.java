/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas2;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class mainTugas2 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Gaji gajiPegawai = new Gaji();
        boolean checkInput = true;

        System.out.print("Masukkan jumlah pegawai yang akan diinput = ");
        int jmlPegawai = input.nextInt();
        System.out.println();

        for (int i = 0; i < jmlPegawai; i++) {
            
            String nip;

            System.out.print("Masukkan NIP = ");
            nip = input.next();
            while (nip.length() != 9 || !nip.matches("[0-9]*")) {
                System.out.println("Input harus Angka dan berjumlah 9 Angka\n");
                System.out.println("Masukkan NIP = ");
                nip = input.next();
            }

            System.out.print("Masukkan nama = ");
            String nama = input.next();
            

            System.out.print("Masukkan gaji = ");
            double gaji = input.nextDouble();

            gajiPegawai.inputPegawai(nip, nama, gaji);

        }

        System.out.println("Masukkan Bulan = ");
        String bulan = input.next();
       
        gajiPegawai.showData();
        gajiPegawai.gaji(bulan);
    }
}
