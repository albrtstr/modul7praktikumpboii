/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas2;

/**
 *
 * @author ASUS
 */
public class Pegawai {

    private String NIP;
    private String Nama;
    private double GajiPerHari;

    public Pegawai() {

    }

    public Pegawai(String NIP, String Nama, double GajiPerHari) {
        this.NIP = NIP;
        this.Nama = Nama;
        this.GajiPerHari = GajiPerHari;
    }

    public String getNIP() {
        return NIP;
    }

    public void setNIP(String NIP) {
        this.NIP = NIP;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String Nama) {
        this.Nama = Nama;
    }

    public double getGajiPerHari() {
        return GajiPerHari;
    }

    public void setGajiPerHari(double GajiPerHari) {
        this.GajiPerHari = GajiPerHari;
    }

    
}
