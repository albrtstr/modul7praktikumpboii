/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas2;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;

/**
 *
 * @author ASUS
 */
public class Gaji {

    ArrayList<Pegawai> pegawai;
    //Pegawai temp;

    public Gaji() {
        pegawai = new ArrayList<Pegawai>();
    }

    public void inputPegawai(String NIP, String Nama, double GajiPerHari) {
        pegawai.add(new Pegawai(NIP, Nama, GajiPerHari));
    }

    public void showData() {
        //for (int i = 0; i < pegawai.size(); i++) {
            //equivalent dengan for pada line 34
        //}
        //for ini dinamakan for-each loop
        //keuntungannya adalah penggunaan for-each loop lebih mempersingkat sintaks
        for (Pegawai temp : pegawai) {
            System.out.println("NIP = " + temp.getNIP()
                    + ", Nama = " + temp.getNama());
        }
    }

    public static int numberOfDaysInAMonth(int month, int year) {
        Calendar monthStart = new GregorianCalendar(year, month, 1);
        return monthStart.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public void gaji(String bln) {
        double GajiPerBulan;
        String bul = bln.toLowerCase();
        int bulan;
        switch (bul) {
            case "januari":
                bulan = 0;
                break;
            case "februari":
                bulan = 1;
                break;
            case "maret":
                bulan = 2;
                break;
            case "april":
                bulan = 3;
                break;
            case "mei":
                bulan = 4;
                break;
            case "juni":
                bulan = 5;
                break;
            case "juli":
                bulan = 6;
                break;
            case "agustus":
                bulan = 7;
                break;
            case "september":
                bulan = 8;
                break;
            case "oktober":
                bulan = 9;
                break;
            case "november":
                bulan = 10;
                break;
            case "desember":
                bulan = 11;
                break;
            default:
                throw new AssertionError();
        }
        for (Pegawai gaji : pegawai) {
            GajiPerBulan = gaji.getGajiPerHari() * numberOfDaysInAMonth(bulan, 2020);
        }

    }
}
